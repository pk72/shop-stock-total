// {{{ Structures, typedefs and classes
// {{{ Shops
class Shops {
public:
    Shops(const std::string& shop_groups_filename,
            const std::string& groups_moves_filename);
    const std::vector<int>& getToShops(int shop_id) const;
private:
    std::unordered_map<int, std::vector<int>> shops_to_shops;
};
// }}}
// {{{ ItemStockStruct
struct ItemStockStruct {
    int totalAmount;
    int totalAmount10k;
};
// }}}
// {{{ ShopsStocksProcessor
class ShopsStocksProcessor {
public:
    ShopsStocksProcessor(const Shops& shops, const std::string& stock_filename,
            const std::string& out_filename);
private:
    void save_item_stock(std::ofstream& out, const std::string& item_id,
        const std::unordered_map<int, ItemStockStruct>& item_stock);
};
// }}}
// }}}
// {{{ Functions
// {{{ generic
// {{{ skipline
std::istream &skipline(std::istream &in);
// }}}
// }}}
// }}}
