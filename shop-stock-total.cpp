// {{{ standard and other includes
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <locale>
#include <fstream>
#include <string>
#include <string.h>
#include <sstream>
#include <assert.h>
#include <ctype.h>
#include <algorithm>
#include <unordered_map>
#include <vector>
#include <list>
#include <set>
// }}}
// {{{ filenames
const char *SHOP_GROUP_MOVES_FILE = "sgm.csv";
const char *SHOP_GROUPS_FILE = "shopgroups.csv";
const char *SHOP_STOCKS_FILE = "shopstock_all.csv";
const char *SHOP_STOCK_TOTALS_FILE = "shop_stock_total.csv";
// }}}

#define MAJOR "1"
#define MINOR "01"
#include "shop-stock-total.h"

// {{{ Shops
// {{{ Shops::Shops
Shops::Shops(const std::string& shop_groups_filename,
            const std::string& groups_moves_filename)
{
    int shop_id, group_id;

    // temporary storage for shops and groups
    std::unordered_map<int, int> shops;
    std::unordered_map<int, std::vector<int>> groups;

    // read file and store shops and groups
    std::ifstream shop_groups_file{shop_groups_filename};
    skipline(shop_groups_file);
    int magic_group_id = 100000;
    while (shop_groups_file >> shop_id >> group_id) {
        if (shop_id == 0) continue;
        if (group_id == 0) group_id = magic_group_id++;
        shops[shop_id] = group_id;
        groups[group_id].push_back(shop_id);
    }

    // add shops from the same group
    for (const auto& shop: shops) {
        shops_to_shops[shop.first].insert(shops_to_shops[shop.first].end(),
                groups[shops[shop.first]].begin(),
                groups[shops[shop.first]].end());
    }

    // read group moves file and add toShops from the toGroup into
    // all corresponding fromGroup shops
    int groupFrom, groupTo;
    std::ifstream group_moves_file{groups_moves_filename};
    skipline(group_moves_file);
    while (group_moves_file >> groupFrom >> groupTo) {
        for (auto shop_id : groups[groupFrom]) {
            shops_to_shops[shop_id].insert(shops_to_shops[shop_id].end(),
                    groups[groupTo].begin(),
                    groups[groupTo].end());
        }
    }

    // erase shop itself from it's own list,
    // sort and remove dupes
    for (auto& shop: shops_to_shops) {
        // de duping via set and back to vector
        std::set<int> s(
            shop.second.begin(),
            shop.second.end()
        );

        //assign back to vector
        shop.second.assign(s.begin(), s.end());
    }

}
// }}}
// {{{ Shops::getToShops
const std::vector<int>& Shops::getToShops(int shop_id) const
{
    return shops_to_shops.at(shop_id);
}
// }}}
// }}}
// {{{ ShopsStocksProcessor
// {{{ ProcessShopStocks
ShopsStocksProcessor::ShopsStocksProcessor(const Shops& shops,
        const std::string& stock_filename,
        const std::string& out_filename)
{
    std::string item_id, item_prev_id = "";
    int shop_id, amount;
    std::ifstream stock_file{stock_filename};
    std::ofstream out{out_filename};

    std::unordered_map<int, ItemStockStruct> item_stock;

    // reading file
    skipline(stock_file);
    while (stock_file >> item_id >> shop_id >> amount) {

        // if item has changed than output the previous one
        if (item_id.compare(item_prev_id) != 0) {
            save_item_stock(out, item_prev_id, item_stock);
            item_stock.clear();
            item_prev_id = item_id;
        }

        for (const auto s: shops.getToShops(shop_id)) {
            item_stock[s].totalAmount += amount;
            if (shop_id < 10000) item_stock[s].totalAmount10k += amount;
        }
    }

    if (item_id.length() > 0) {
        save_item_stock(out, item_id, item_stock);
    }

}
// }}}
// {{{ save_item_stock
void ShopsStocksProcessor::save_item_stock(
		std::ofstream& out,
		const std::string& item_id,
        const std::unordered_map<int, ItemStockStruct>& item_stock)
{
    for (const auto& [id, value]: item_stock) {
        out << item_id << "\t" << id << "\t"
            << value.totalAmount << "\t"
            << value.totalAmount10k << "\n";
    }
}
// }}}
// }}}
// {{{ skipline (thx Edward from CodeReview @ StackExchange
std::istream &skipline(std::istream &in)
{
    return in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}
// }}}
// {{{ main
int main()
{
	std::cout << "shop-stock-total v" << MAJOR << "." << MINOR << "."
		<< BUILDNUMBER << " from " << BUILDDATE << "\n";

    ShopsStocksProcessor(
            Shops(SHOP_GROUPS_FILE, SHOP_GROUP_MOVES_FILE),
            SHOP_STOCKS_FILE, SHOP_STOCK_TOTALS_FILE);

    return 0;
}
// }}}
