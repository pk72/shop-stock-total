# coding=utf-8

import codecs
import os


# load files from the current directory
SOURCE_PATH = "."


# {{{ load_file
def load_textfile_utf8(sFileName):
    fn = os.path.join(SOURCE_PATH, sFileName)
    print("loading " + fn + " ...")
    f = codecs.open(fn, mode='r', encoding='utf8')
    ft = f.read()
    f.close()
    return ft
# }}}
