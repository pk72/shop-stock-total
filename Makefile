PROGRAM = shop-stock-total
BUILD_NUMBER_FILE=build-number.txt
CXX=g++
CXXFLAGS := -g -std=c++17 -Wall -O3 -march=native -mtune=native -DBUILDDATE="$$(date +'%Y%m%d')" -DBUILDNUMBER=$$(cat $(BUILD_NUMBER_FILE))
LDFLAGS := -g -L /usr/lib:/usr/lib/x86_64-linux-gnu

all: $(PROGRAM)

$(PROGRAM): $(PROGRAM).gcda $(PROGRAM).o $(BUILD_NUMBER_FILE)
	$(CXX) $(CXXFLAGS) -fprofile-use -o $@ $(PROGRAM).o $(LDFLAGS)

$(PROGRAM).gcda: profile/$(PROGRAM)
	./profile/$(PROGRAM) >/dev/null
	cp profile/$(PROGRAM).gcda ./

profile/$(PROGRAM): $(PROGRAM).cpp $(PROGRAM).h $(BUILD_NUMBER_FILE)
	if [ ! -d profile ]; then mkdir profile; fi
	cp $(PROGRAM).cpp profile/
	cp $(PROGRAM).h profile/
	cp $(BUILD_NUMBER_FILE) profile/
	cd profile && $(CXX) -c $(CXXFLAGS) -fprofile-generate $(PROGRAM).cpp
	cd profile && $(CXX) $(CXXFLAFS) -fprofile-generate -o $(PROGRAM) $(PROGRAM).o $(LDFLAGS)


$(BUILD_NUMBER_FILE): $(PROGRAM).cpp
	@if ! test -f $(BUILD_NUMBER_FILE); then echo 0 > $(BUILD_NUMBER_FILE); fi
	@echo $$(($$(cat $(BUILD_NUMBER_FILE)) + 1)) > $(BUILD_NUMBER_FILE)

shop-stock-total.o: $(PROGRAM).cpp $(PROGRAM).h $(BUILD_NUMBER_FILE)
	$(CXX) -c $(CXXFLAGS) -fprofile-use $(PROGRAM).cpp

clean:
	@rm -rf *.o $(PROGRAM) ./profile *.gcda

# the following line works
# gcc -L/usr/lib shop-stock-total.c -lm
