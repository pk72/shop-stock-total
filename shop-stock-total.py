# coding=utf-8
# -*- coding: utf-8 -*-

import codecs
import sys

import FileTools.filetools

resfile = codecs.open("shop_stock_total.csv", mode="w+", encoding='utf-8')
resfile.write("item_idx\tshop_id\ttotal\ttotal_10k\n")


sgm_arr = {} 
# {{{ ShopGroupMove
class ShopGroupMove:
    group_to = 0
    
    def __init__(self):
        self.groups_from = []

    def key(self):
        return str(self.group_to)

    def out(self):
        print(str(self.group_to) + ' <== ' + ','.join(map(str, sorted(self.groups_from))))
        return
# }}}

sg_arr = {}
# {{{ ShopGroup
class ShopGroup:
    id = 0
    
    def __init__(self):
        self.shops = []

    def key(self):
        return str(self.id)

    def out(self):
        print(str(self.id) + ': ' + ','.join(map(str, self.shops)))
# }}}

shop_arr = {}
# {{{ Shop
class Shop:
    id = 0
    group = 0
#    shops_in_group = []

    def __init__(self):
        self.from_shops_all = []
        self.from_shops_10k = []

    def key(self):
        return str(self.id)
# }}}

# {{{ Availability
class Availability:
    shop_id = 0
    available = 0
    total_available = 0
    total_available_10k = 0

    def key(self):
        return str(self.shop_id)
# }}}

shop_stock_arr = {}
# {{{ ShopStock
class ShopStock:
	item_idx = ""

        def __init__(self):
            self.availability = {} 

	def key(self):
		return self.item_idx
# }}}

# loading shop_group_moves as text 
sgmt = FileTools.filetools.load_textfile_utf8("sgm.csv")

# {{{ process_sgmt - Shop Group Moves text
def process_sgmt(sText):
    lines = sText.split("\n")
    n = 0
    for line in lines:
        if n == 0:
            n += 1
            continue

        if len(line) > 3:
            sgm = None
            items = line.split("\t")
            group_from = int(items[0])
            group_to = int(items[1])
            print("%d -> %d" % (group_from, group_to))

            if str(group_to) in sgm_arr.keys():
                sgm_arr[str(group_to)].groups_from.append(group_from)

            else:
                sgm = ShopGroupMove()
                sgm.group_to = group_to
                sgm.groups_from.append(group_from)
                # print(sgm.key())
                # sgm.out()
                sgm_arr[str(sgm.key())] = sgm

# }}}

process_sgmt(sgmt)
for a in sgm_arr.values():
    a.out()

st = FileTools.filetools.load_textfile_utf8("shopgroups.csv")

# {{{ process_st - shops and shopgroups text
def process_st(sText):
    lines = sText.split("\n")
    n = 0
    for line in lines:
        if n == 0:
            n += 1
            continue

        if len(line) > 3:
            items = line.split("\t")
            shop = Shop()
            shop.id = int(items[0])
            shop.group = int(items[1])

            if shop.id < 1:
                continue

            shop_arr[str(shop.key())] = shop

            if shop.group > 0:
                if str(shop.group) in sg_arr.keys():
                    sg_arr[str(shop.group)].shops.append(shop.id)
                else:
                    sg = ShopGroup()
                    sg.id = shop.group
                    sg.shops.append(shop.id)
                    sg_arr[str(sg.key())] = sg

# }}}


process_st(st)
for a in sg_arr.values():
    a.out()
print("total shops loaded " + str(len(shop_arr)))
for s in shop_arr.values():
    print(s.id)

# {{{ fill__from_shops_all
def fill__from_shops_all():

    for s in shop_arr.values():

        # fill shops from the same group
        if str(s.group) in sg_arr.keys():
            s.from_shops_all.extend(sg_arr[str(s.group)].shops)

        # fill shops from movement groups
        if str(s.group) in sgm_arr.keys():
            for g in sgm_arr[str(s.group)].groups_from:
                if str(g) in sg_arr.keys():
                    s.from_shops_all.extend(sg_arr[str(g)].shops)


        # de-duping
        s.from_shops_all = sorted(list(set(s.from_shops_all)))

        # remove shop itself
        if s.id in s.from_shops_all:
            s.from_shops_all.remove(s.id)

        s.from_shops_10k = [x for x in s.from_shops_all if x <10000]
    return
# }}}

fill__from_shops_all()

for s in shop_arr.values():
    print(str(s.id) + ' <= ' + ','.join(map(str, s.from_shops_10k)))
    

#ssfile = codecs.open("shopstock_all.csv", mode="r", encoding='utf-8')
sst = FileTools.filetools.load_textfile_utf8("shopstock_all.csv")


# {{{ print_av
def print_av(it):
    print(it.item_idx)

    for av in it.availability.values():
        print("%d: %d %d %d" % (av.shop_id, av.available, av.total_available,\
                av.total_available_10k))

    return
# }}}

x=0
# {{{ process_item
def process_item(it):
    global x
#    print_av(it)

    # add empty shops to availability list
    #  coz items could be moved there from other shops
    for s in shop_arr.values():
        if str(s.id) not in it.availability.keys():
            if len(s.from_shops_all) > 0:
                av = Availability()
                av.shop_id = s.id
                av.available = 0
                it.availability[str(av.key())] = av

    # summing up total availability
    for av in it.availability.values():

        # amount from shop itself
        av.total_available = av.available
        av.total_available_10k = av.available

        # sum up for all shops which moves here
        for sh in shop_arr[str(av.shop_id)].from_shops_all:
            if str(sh) in it.availability.keys():
                av.total_available += it.availability[str(sh)].available

        # sum up for all shops (below 10k) which moves here
        for sh in shop_arr[str(av.shop_id)].from_shops_10k:
            if str(sh) in it.availability.keys():
                av.total_available_10k += it.availability[str(sh)].available

    # we might still have blank amounts\
    #   so -> filter em out
    # it.availability = { k: v for k,v in it.availability.iteritems() if v.total_available + v.total_available_10k > 0  }

#    print_av(it)

    write_result(it)
    # x += 1
    # if x>3:
    #     exit(0)
    return
# }}}


# {{{ write_result
def write_result(it):
    global resfile
    for av in it.availability.values():
        if av.total_available + av.total_available_10k == 0:
            continue

        resfile.write(it.item_idx + "\t" + str(av.shop_id) + "\t"  + \
                str(av.total_available) + "\t" + \
                str(av.total_available_10k) + "\n")

# }}}


# {{{ process_sst - ShopStock text 
def process_sst(t):
    lines = t.split("\n")
    n = 0
    
    it = ShopStock()
    for line in lines:
        if n == 0:
            n += 1
            continue

        if len(line) < 3:
            continue

        items = line.split("\t")
        
        item_idx = items[0]
        av = Availability()
        av.shop_id = int(items[1])
        av.available = int(items[2])

        if it.item_idx <> item_idx:

            # process previous item
            if len(it.item_idx)>1:
                process_item(it)

            # create new one
            it = ShopStock()
            it.item_idx = item_idx

        # append availability
        it.availability[str(av.key())] = av



    # process last item
    process_item(it)

# }}}

process_sst(sst)

resfile.close
